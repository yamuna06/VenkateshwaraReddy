import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import { URL } from '../../model/url'
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-eventdetails',
  templateUrl: 'eventdetails.html',
})
export class EventdetailsPage {
  urls(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  currentEvents: any;
  eventdetails : any;
  users: any;
  link: string;
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing) {
    this.link = 'http://s1avreddy.magnyfied.com/img/'
    this.eventdetails = this.navParams.get('evt');
    /* console.log(this.eventdetails); */

  }
  /* ionViewDidEnter() {
    this.service.fetch(URL.EVENTS)
      .subscribe((x) => {
        this.users = x.e;
        console.log(this.users);
      
      })
      
     
  } */

  share(item) {
    this.socialSharing.share('', '', `${this.link}${this.eventdetails.img}`, '')
      .then(data => {
        console.log(this.urls);
      }).catch(() => {

      })
  }

}


