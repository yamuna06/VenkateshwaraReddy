import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import { URL } from '../../model/url'
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-newsdetails',
  templateUrl: 'newsdetails.html',
})
export class NewsdetailsPage {


  
  urls(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  
  eventdetails: any;
  selectedDate: any;
  users: any;
  link: string;
  newsdetails:any;
  
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing) {
    this.link = 'http://s1avreddy.magnyfied.com/img/'
    this.newsdetails = this.navParams.get('evt1');
    console.log(this.newsdetails);
    
    

  }
  ionViewDidEnter() {
    this.service.fetch(URL.EVENTS)
      .subscribe((x) => {
        this.users = x.e;
        console.log(this.users);
      })
     
  }
  share(item) {
    this.socialSharing.share('', '', `${this.link}${this.newsdetails.img}`, '')
      .then(data => {  alert("Image Shared");
        
      }).catch(() => {
        alert("Unable Shared");
      })
  }

  
}

