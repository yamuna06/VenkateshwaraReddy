import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the ManifestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manifest',
  templateUrl: 'manifest.html',
})
export class ManifestPage {

  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  users: any;

  ionViewDidEnter() {
    this.service.fetch(URL.ABOUTUS)
      .subscribe((x) => {
        this.users = x;
        console.log(this.users);
      })

      
  }

}
