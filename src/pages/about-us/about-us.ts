import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the AboutUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUsPage {

  link: string;
  
  constructor(public service: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.link ='http://s1avreddy.magnyfied.com/img/'
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUsPage');
  }
  users: any;

  ionViewDidEnter() {
    this.service.fetch(URL.ABOUTUS)
      .subscribe((x) => {
        this.users = x;
        console.log(this.users);
      })

      
  }

}
