import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalleadersPage } from './localleaders';

@NgModule({
  declarations: [
    LocalleadersPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalleadersPage),
  ],
})
export class LocalleadersPageModule {}
