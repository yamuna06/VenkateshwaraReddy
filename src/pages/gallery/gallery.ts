import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {

  openimg: any;
  item(arg0: any, arg1: any): any {
    throw new Error("Method not implemented.");
  }
  photos: any;
  imagesList: { url: any; };
  images = [];
  storageDirectory: string;
  fileName: string;
  imageSrc: any;
  ios: any;
  users: any;
  photo: any;


  link: string;
  urls: any;
  albums: string;

  constructor(public service: ServiceProvider, public navCtrl: NavController,
    private file: File, private transfer: FileTransfer, public platform: Platform,
    public navParams: NavParams, private socialSharing: SocialSharing, public photoLibrary: PhotoLibrary) {
    this.link = 'http://s1avreddy.magnyfied.com/img/'
    this.images = this.navParams.get('images');
    console.log("images", this.images)
    // this.images.forEach(x => {
    //   this.imagesList = { url: x.image }
    // //  this.photos.push(this.imagesList)
    // });
  }




  share(item) {
    this.socialSharing.share('', '', `${this.link}${item.surl}`, '')
      .then(data => {
        console.log(this.urls);
      }).catch(() => {

      })
  }

  ionViewDidEnter() {
    this.service.fetch(URL.GALLERY)
      .subscribe((x) => {
        this.users = x.albums;
        console.log(this.users);
      })
  }



  download(item) {
    let path = null;
    if (this.platform.is(this.ios)) {
      path = this.file.documentsDirectory;
    }
    else {
      path = this.file.dataDirectory;

    }

    const transfer = this.transfer.create();

    const url = encodeURI(`${this.link}${item.surl}`);
    this.fileName = 'img' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.jpeg'
    this.storageDirectory = this.file.externalDataDirectory;
    transfer.download(url, this.storageDirectory + this.fileName).then((entry) => {
      alert('Image Downloaded')
      console.log('success');
    }, (error) => {
      alert('Error')
      console.log('error');
    });


  }


  /////////////////////////////////////////////////////////////////////


  // download(item){
  //     const fileTransfer: FileTransferObject = this.transfer.create();
  // // var url=`${cordova.file.applicationDirectory}www/assets/img/${Image}`;
  // const url = encodeURI(`${this.link}${item.surl}`);
  //      var uri = encodeURI(url);
  //     // var filepath = this.file.cacheDirectory+ '/' + this.vacancy.attachment;//("/"+this.vacancy.attachment);
  //      fileTransfer.download(uri,this.file.dataDirectory + "test.png").then((entry) => {
  //      alert('image downloaded');

  //          console.log('download complete: ' + entry );
  //          this.imageSrc = entry.toUrl();
  //          console.log(this.imageSrc);

  //        }).catch(error => {
  //         alert("Image NotDownloaded");
  //          console.log(JSON.stringify(error));

  //        });

  //   }

  // download(item) {
  //   this.platform.ready().then(() => {

  //     const fileTransfer: FileTransferObject = this.transfer.create();

  //     var url = encodeURI(`${this.link}${item.surl}`);


  //     fileTransfer.download(url, this.file.dataDirectory + "test.png").then(entry => {
  //       alert('image downloaded');

  //       console.log('download complete: ' + entry);
  //       this.imageSrc = entry.toUrl();
  //       console.log(this.imageSrc);

  //     }).catch(error => {
  //       alert("Image NotDownloaded");
  //       console.log(JSON.stringify(error));

  //     });

  //   });

  // }


  selectedImage(item) {

    this.navCtrl.push('GallaryimagesPage', { 'openimg': item })

    console.log("openimg:", this.openimg)
    console.log("item",this.item)

  }

}