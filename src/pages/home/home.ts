import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import { URL } from '../../model/url'

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  background: any;
  dashboarddata: any;
  constructor(public navCtrl: NavController, public service: ServiceProvider, ) {


  }
  navigateTo(item) {
    if (item.act == "feed")
      this.navCtrl.push('NewsPage');
    else if (item.act == "events")
      this.navCtrl.push('EventsPage');
    else if (item.act == "issues")
      this.navCtrl.push('CurrentproblemsPage');
    else if (item.act == "about-us")
      this.navCtrl.push('ProfilePage');
    else if (item.act == "gallery")
      this.navCtrl.push('GallerySliderPage');
    else if (item.act == "missed-call")
      this.navCtrl.push('CallNumberPage');
    else if (item.act == "leaders")
      this.navCtrl.push('LocalleadersPage');
  }
  ionViewDidEnter() {
    this.service.fetch(URL.DASHBOARD)
      .subscribe((x) => {
        this.background=x.d_img
        console.log(x);
        this.dashboarddata = x.d;
       
        console.log("bgImg:",this.background)
      })
  }

}