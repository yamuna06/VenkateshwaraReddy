import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../Providers/service';
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular';

import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-gallery-slider',
  templateUrl: 'gallery-slider.html',
})
export class GallerySliderPage {

  @ViewChild(Slides) slides: Slides;


  storageDirectory: string;
  fileName: string;
  imageSrc: any;
  ios: any;
  users: any;
  photo: any;

  images=[];
  link: string;
  urls: any;
  albums: string;
  event=[];
  constructor(public service: ServiceProvider, public navCtrl: NavController,
    private viewchild: ViewChild, 
    private file: File, private transfer: FileTransfer, public platform: Platform,
    public navParams: NavParams, private socialSharing: SocialSharing, public photoLibrary: PhotoLibrary) {
    this.link = 'http://s1avreddy.magnyfied.com/img/'

  }openGallery() {
    this.navCtrl.push('GalleryPage');
  }
  
  ionViewDidEnter() {
    this.service.fetch(URL.GALLERY)
      .subscribe((x) => {
        this.users = x.albums;
        console.log(this.users);
       
      })
  }
  openphotos(parameter){

    this.navCtrl.push("GalleryPage",{images:parameter});
    console.log("parameter",parameter);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
  }
  goToSlide() {
    this.slides.slideTo(2, 500);
  }
}