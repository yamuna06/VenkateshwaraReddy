import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GallerySliderPage } from './gallery-slider';

@NgModule({
  declarations: [
    GallerySliderPage,
  ],
  imports: [
    IonicPageModule.forChild(GallerySliderPage),
  ],
})
export class GallerySliderPageModule {}
