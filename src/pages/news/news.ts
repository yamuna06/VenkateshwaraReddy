import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../Providers/service"
import 'rxjs'
import { URL } from '../../model/url'
import { Observable } from 'rxjs/Observable';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {

  evt1: (item: any) => void;
  newsdetails: any;
  urls: any;
  link: string;
  constructor(public service: ServiceProvider, public navCtrl: NavController,private socialSharing: SocialSharing, public navParams: NavParams) {
    this.link = 'http://s1avreddy.magnyfied.com/img/'
    
  }


  users= [];

  ionViewDidEnter() {
    this.service.fetch(URL.NEWS)
      .subscribe((x) => {
        this.users = x.n;
        console.log(this.users);
      })
  }
  news(item){
    this.navCtrl.push('NewsdetailsPage',{'evt1':item})
    console.log("item",item)
    console.log("evt1",this.evt1)
    // this.news= this.evt1;
  }

  
  share(item) {
      this.socialSharing.share('', '', `${this.link}${item.img}`, '')
        .then(data => { alert("Image uploaded");
          
          console.log("data",data);
        }).catch(() => {
  

        })
    }
  

  
}




