import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GallaryimagesPage } from './gallaryimages';

@NgModule({
  declarations: [
    GallaryimagesPage,
  ],
  imports: [
    IonicPageModule.forChild(GallaryimagesPage),
  ],
})
export class GallaryimagesPageModule {}
