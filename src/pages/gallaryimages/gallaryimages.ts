import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import 'rxjs/Rx';
import { Platform } from 'ionic-angular';




declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-gallaryimages',
  templateUrl: 'gallaryimages.html',
})
export class GallaryimagesPage {

  fileName: string;
  ios: any;
  imageSrc: any;
  vacancy: any;
  http: any;
  downloadFile(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  _reportService: any;
  FileTransfer: any;
  alertCtrl: any;
  open:any;
  link: string;
  users: any;
  photo: any;
  storageDirectory: string = '';


  urls: any;
  albums: string;


  constructor(public platform: Platform, public navCtrl: NavController,
    public navParams: NavParams, private file: File, private socialSharing: SocialSharing,
    public photoLibrary: PhotoLibrary, private transfer: FileTransfer) {
    this.link = 'http://s1avreddy.magnyfied.com/img/'
    this.open = this.navParams.get('openimg')
    console.log("open", this.open)
  }



  download(item) {
    let path = null;
    if (this.platform.is(this.ios)) {
      path = this.file.documentsDirectory;
    }
    else {
      path = this.file.dataDirectory;

    }

    const transfer = this.transfer.create();

    const url = encodeURI(`${this.link}${item.surl}`);
    this.fileName='img' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.jpeg'
    this.storageDirectory=this.file.externalDataDirectory;
    transfer.download(url, this.storageDirectory+this.fileName).then((entry) => {
      alert('success')
      console.log('success');
    }, (error) => {
      alert('Error')
      console.log('error');
    });


  }
  /////////////////////////////////////////////////////////////////////
  //   download(item){
  //     const fileTransfer: FileTransferObject = this.transfer.create();
  // // var url=`${cordova.file.applicationDirectory}www/assets/img/${Image}`;
  // const url = encodeURI(`${this.link}${item.surl}`);
  //      var uri = encodeURI(url);
  //     // var filepath = this.file.cacheDirectory+ '/' + this.vacancy.attachment;//("/"+this.vacancy.attachment);
  //      fileTransfer.download(uri,this.file.dataDirectory + "test.png").then((entry) => {
  //      alert('image downloaded');

  //          console.log('download complete: ' + entry );
  //          this.imageSrc = entry.toUrl();
  //          console.log(this.imageSrc);

  //        }).catch(error => {
  //         alert("Image NotDownloaded");
  //          console.log(JSON.stringify(error));

  //        });

  //   }

  // download(item) {
  //   this.platform.ready().then(() => {

  //     const fileTransfer: FileTransferObject = this.transfer.create();

  //     var url = encodeURI(`${this.link}${item.surl}`);


  //     fileTransfer.download(url, this.file.dataDirectory + "test.png").then(entry => {
  //       alert('image downloaded');

  //       console.log('download complete: ' + entry);
  //       this.imageSrc = entry.toUrl();
  //       console.log(this.imageSrc);

  //     }).catch(error => {
  //       alert("Image NotDownloaded");
  //       console.log(JSON.stringify(error));

  //     });

  //   });

  // }

  share(item) {
    this.socialSharing.share('', '', `${this.link}${item.surl}`, '')
      .then(data => {
        console.log(this.urls);
      }).catch(() => {

      })
  }

  // download(item){
  //   var blob = new Blob([`${this.link}${item.surl}`], );
  //   var url= window.URL.createObjectURL(blob);
  //   this.photoLibrary.saveImage(url, this.albums).then((entry) => {
  //      alert('image downloaded');

  //          console.log('download complete: ' + entry );

  //          console.log(this.imageSrc);

  //        }).catch(error => {
  //         alert("Image NotDownloaded");
  //          console.log(JSON.stringify(error));

  //        });

  // } 


  //   download() {
  //     const url = '${this.link}${item.surl}';
  //     FileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
  //       console.log('download complete: ' + entry.toURL());
  //     }
}
