import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrentproblemsPage } from './currentproblems';

@NgModule({
  declarations: [
    CurrentproblemsPage,
  ],
  imports: [
    IonicPageModule.forChild(CurrentproblemsPage),
  ],
})
export class CurrentproblemsPageModule {}
