import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { CallNumber } from '@ionic-native/call-number';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceProvider } from '../Providers/service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { CalendarModule } from 'ionic3-calendar-en';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

@NgModule({
  declarations: [
    MyApp,

  ],
  imports: [
    BrowserModule,
    CalendarModule,
    IonicModule.forRoot(MyApp), HttpModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

  ],
  providers: [
    StatusBar,
    SplashScreen, HttpModule, ServiceProvider, CallNumber,File,
    { provide: ErrorHandler, useClass: IonicErrorHandler }, SocialSharing,
    PhotoLibrary,
    FileTransfer,
    ViewChild,
    Slides
  ]
})
export class AppModule { }
